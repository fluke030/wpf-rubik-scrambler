﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using WPFRubik.Model;

namespace WPFRubik.ViewModel;

public class ScrambleViewModel : ObservableObject
{
    private Scramble _scramble;

    public ScrambleViewModel()
    {
        _scramble = new Scramble();
        Scramble2x2Command = new RelayCommand(Scramble2x2);
        Scramble3x3Command = new RelayCommand(Scramble3x3);
        Scramble4x4Command = new RelayCommand(Scramble4x4);
        Scramble5x5Command = new RelayCommand(Scramble5x5);
        Scramble6x6Command = new RelayCommand(Scramble6x6);
        Scramble7x7Command = new RelayCommand(Scramble7x7);
        ScrambleSkewbCommand = new RelayCommand(ScrambleSkewb);
        ScrambleMegaminxCommand = new RelayCommand(ScrambleMegaminx);
        ScramblePyraminxCommand = new RelayCommand(ScramblePyraminx);
    }

    public Scramble Scramble
    {
        get => _scramble;
        set
        {
            SetProperty(ref _scramble, value);
        }
    }

    public IRelayCommand Scramble2x2Command { get; }
    public IRelayCommand Scramble3x3Command { get; }
    public IRelayCommand Scramble4x4Command { get; }
    public IRelayCommand Scramble5x5Command { get; }
    public IRelayCommand Scramble6x6Command { get; }
    public IRelayCommand Scramble7x7Command { get; }
    public IRelayCommand ScrambleSkewbCommand { get; }
    public IRelayCommand ScrambleMegaminxCommand { get; }
    public IRelayCommand ScramblePyraminxCommand { get; }

    private List<string> GenerateScramble(int maxSteps, string[,] steps, bool insert10 = false)
    {
        var turnsToMake = new List<string>();
        var rand = new Random();
        var priorIndex = -1;
        var lengthSteps = steps.GetUpperBound(1) + 1;
        var depthSteps = steps.GetUpperBound(0) + 1;
        int currentIndex;
        for (var i = 0; i < maxSteps; i++)
        {
            do
            {
                currentIndex = rand.Next(lengthSteps);
            } while (priorIndex == currentIndex);
            priorIndex = currentIndex;
            var outer = rand.Next(depthSteps);
            var ourStep = steps[outer, currentIndex];
            turnsToMake.Add(ourStep);
            if (insert10 && (i % 10 == 9))
            {
                turnsToMake.Add("U'");
            }
        }
        return turnsToMake;
    }

    private void UpdateScramble(string typeName, List<string> turns, int numSteps)
    {
        _scramble.Type = typeName;
        _scramble.Turns.Clear();
        turns.ForEach(t =>
        {
            _scramble.Turns.Add(t);
        });
        _scramble.NumberOfTurns = numSteps;
    }

    private void Scramble2x2()
    {
        var steps = new string[,] { { "F", "R", "L" },
                                    { "F'", "R'", "L'" },
                                    { "F2", "R2", "L2" }};
        int numSteps = 10;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("2x2", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void Scramble3x3()
    {
        var steps = new string[,] { { "R", "L", "U", "B", "F", "D" },
                                    { "R'", "L'", "U'", "B'", "F'", "D'" },
                                    { "R2", "L2", "U2", "B2", "F2", "D2" } };
        int numSteps = 20;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("3x3", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void Scramble4x4()
    {
        var steps = new string[,] { { "F", "B", "L", "R", "U", "D" },
                                    { "F'", "B'", "L'", "R'", "U'", "D'" },
                                    { "F2", "B2", "L2", "R2", "U2", "D2" },
                                    { "FW", "BW", "LW", "RW", "UW", "DW" },
                                    { "FW'", "BW'", "LW'", "RW'", "UW'", "DW'" },
                                    { "FW2", "BW2", "LW2", "RW2", "UW2", "DW2" } };
        int numSteps = 40;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("4x4", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void Scramble5x5()
    {
        var steps = new string[,] { { "R", "L", "U", "B", "F", "D" },
                                    { "R'", "L'", "U'", "B'", "F'", "D'" },
                                    { "R2", "L2", "U2", "B2", "F2", "D2" },
                                    { "RW", "LW", "UW", "BW", "FW", "DW" },
                                    { "RW'", "LW'", "UW'", "BW'", "FW'", "DW'" },
                                    { "RW2", "LW2", "UW2", "BW2", "FW2", "DW2" }};
        int numSteps = 60;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("5x5", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void Scramble6x6()
    {
        var steps = new string[,] { { "R", "L", "U", "B", "F", "D" },
                                    { "Rw", "Lw", "Uw", "Bw", "Fw", "Dw" },
                                    { "3Rw", "3Lw", "3Uw", "3Bw", "3Fw", "3Dw" },
                                    { "R'", "L'", "U'", "B'", "F'", "D'" },
                                    { "Rw'", "Lw'", "Uw'", "Bw'", "Fw'", "Dw'" },
                                    { "3Rw'", "3Lw'", "3Uw'", "3Bw'", "3Fw'", "3Dw'" },
                                    { "R2", "L2", "U2", "B2", "F2", "D2" },
                                    { "Rw2", "Lw2", "Uw2", "Bw2", "Fw2", "Dw2" },
                                    { "3Rw2", "3Lw2", "3Uw2", "3Bw2", "3Fw2", "3Dw2" } };
        int numSteps = 80;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("6x6", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void Scramble7x7()
    {
        var steps = new string[,] { { "R", "L", "U", "B", "F", "D" },
                                    { "R'", "L'", "U'", "B'", "F'", "D'" },
                                    { "R2", "L2", "U2", "B2", "F2", "D2" },
                                    { "Rw'", "Lw'", "Uw'", "Bw'", "Fw'", "Dw'" },
                                    { "3Rw", "3Lw", "3Uw", "3Bw", "3Fw", "3Dw" },
                                    { "3Rw'", "3Lw'", "3Uw'", "3Bw'", "3Fw'", "3Dw'" },
                                    { "Rw", "Lw", "Uw", "Bw", "Fw", "Dw" },
                                    { "Rw2", "Lw2", "Uw2", "Bw2", "Fw2", "Dw2" },
                                    { "3Rw2", "3Lw2", "3Uw2", "3Bw2", "3Fw2", "3Dw2" } };
        int numSteps = 100;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("7x7", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void ScrambleSkewb()
    {
        var steps = new string[,] { { "F", "R", "L", "D" },
                                    { "F'", "R'", "L'", "D'" }};
        int numSteps = 25;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("Skewb", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void ScrambleMegaminx()
    {
        var steps = new string[,] { { "R++", "D++" },
                                    { "R--", "D--" }};
        int numSteps = 50;
        var insert10 = true;
        var turns = GenerateScramble(numSteps, steps, insert10);
        UpdateScramble("Megaminx", turns, numSteps);
        OnPropertyChanged("Scramble");
    }

    private void ScramblePyraminx()
    {
        var steps = new string[,] { { "U", "L", "R", "B" },
                                    { "U'", "L'", "R'", "B'" },
                                    { "u", "l", "r", "b" },
                                    { "u'", "l'", "r'", "b'" }};
        int numSteps = 20;
        var turns = GenerateScramble(numSteps, steps);
        UpdateScramble("Pyraminx", turns, numSteps);
        OnPropertyChanged("Scramble");
    }
}
