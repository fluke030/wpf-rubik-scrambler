﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WPFRubik.Model;

public class Scramble
{
    public string Type { get; set; } = "";
    public int NumberOfTurns { get; set; }
    public ObservableCollection<string> Turns { get; set; } = new ObservableCollection<string>();
}
