﻿using System.Windows;
using WPFRubik.ViewModel;

namespace WPFRubik
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ScrambleViewModel();
        }
    }
}
